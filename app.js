const app = Vue.createApp({
  data() {
    return {
      userInput: "",
      isVisible: true,
      backgroundColor: "",
    };
  },
  methods: {
    toggleP() {
      this.isVisible = !this.isVisible;
    },
  },
  computed: {
    pClasses() {
      /* 
      // OR
         {
          user1: this.userInput == "user1",
           user2: this.userInput == "user2",
        }
        */
      return [
        {
          user1: this.userInput == "user1",
        },
        {
          user2: this.userInput == "user2",
        },
        {
          visible: this.isVisible,
        },
        {
          hidden: !this.isVisible,
        },
      ];
    },
  },
});
app.mount("#assignment");
